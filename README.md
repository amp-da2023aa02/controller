## Controller for sound amplifier #DA2023AA02.
### Components:
- stm32f103c8t6 (blue pill)
- ssd1306 (128x64)
- jdy-68a (bluetooth audio & serial)
- rotary encoder 
- buttons (6 pcs)
- amplifier itselt (pt2322 & pt2323 based)
- audio card usb to 5.1
- some resistors & transistors
