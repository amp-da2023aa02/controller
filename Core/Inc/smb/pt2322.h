/*
 * pt2322.h
 *
 *  Created on: 22 серп. 2021 р.
 *      Author: symbx
 */

#ifndef INC_SMB_PT2322_H_
#define INC_SMB_PT2322_H_

#define PT2322_ADDR (0x44 << 1)

#define PT2322_SYS_RESET 0xFF
#define PT2322_ACTIVATE 0xC7
#define PT2322_FUNCTIONS 0x70
#define PT2322_VOL_10 0xE0
#define PT2322_VOL_01 0xD0
#define PT2322_TONE_BASS 0x90
#define PT2322_TONE_MIDDLE 0xA0
#define PT2322_TONE_TREBLE 0xB0
#define PT2322_TRIM_FL 0x10
#define PT2322_TRIM_FR 0x20
#define PT2322_TRIM_C 0x30
#define PT2322_TRIM_BL 0x40
#define PT2322_TRIM_BR 0x50
#define PT2322_TRIM_SW 0x60

#define PT2322_MAX_VOL 79
#define PT2322_MAX_TONE 14
#define PT2322_MAX_TRIM 15

#endif /* INC_SMB_PT2322_H_ */
