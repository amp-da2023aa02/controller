/*
 * pt2323.h
 *
 *  Created on: Aug 22, 2021
 *      Author: symbx
 */

#ifndef INC_SMB_PT2323_H_
#define INC_SMB_PT2323_H_

#define PT2323_ADDR (0x4A << 1)
#define PT2323_INPUT 0xC0
#define PT2323_ENCH 0xD0
#define PT2323_MIX 0x90
#define PT2323_MUTE 0xF0

#endif /* INC_SMB_PT2323_H_ */
