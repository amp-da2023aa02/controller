/*
 * tasks.h
 *
 *  Created on: Mar 5, 2022
 *      Author: symbx
 */

#ifndef INC_SMB_TASKS_H_
#define INC_SMB_TASKS_H_

#include <stdint.h>

#define TASKS_MAX      16
#define FLAG_RUN       0x01

typedef void (*func)(void);
typedef struct task {
	func function;
	uint16_t delay;
	uint16_t interval;
	uint16_t flags;
} task;

void tasks_init (void);
void tasks_set (func call, uint16_t delay, uint16_t interval);
void tasks_remove (func call);
void tasks_execute (void);
void tasks_tick (void);


#endif /* INC_SMB_TASKS_H_ */
