#ifndef CONFIG_H
#define CONGIG_H

#include <stdint.h>

#define DEVELOPER_STR "SymbX"
#define APPLICATION_STR "Audio"
#define VOLUME_STR "Volume"
#define TONE_STR "Tone"
#define TRIM_STR "Trim"
#define FRONT_STR "Front"
#define BACK_STR "Back"
#define LEFT_STR "Left"
#define RIGHT_STR "Right"
#define CENTER_STR "Center"
#define SUBWOOFER_STR "SubWoofer"
#define BASS_STR "Bass"
#define MIDDLE_STR "Middle"
#define TREBLE_STR "Treble"
#define MIX_CHNL_STR "Mix chnnl"
#define THIRD_D_STR "3D"
#define MUTE_STR "Mute"
#define ENCHANCE_STR "Enhance"
#define TONE_CTRL_STR "Tone ctrl"
#define BLUETOOTH_STR "Bluetooth"
#define INPUT_STR "Input"
#define AUX_STR "AUX"
#define RCA1_STR "RCA1"
#define RCA2_STR "RCA2"
#define DIRECT_STR "USB"
#define ON_STR "ON"
#define OFF_STR "OFF"
#define SCREEN_CLEAR 5000
#define ROTARY_RESET (SCREEN_CLEAR + 50)
#define CONFIG_APPLY 50
#define CONFIG_SAVE (SCREEN_CLEAR + 100)
#define BUTTON_0 0b00000001
#define BUTTON_1 0b00000010
#define BUTTON_2 0b00000100
#define BUTTON_3 0b00001000
#define BUTTON_4 0b00010000
#define BUTTON_5 0b00100000

typedef struct {
	int8_t bass;
	int8_t middle;
	int8_t treble;
} Tones;

typedef struct {
	uint8_t front_left;
	uint8_t front_right;
	uint8_t back_left;
	uint8_t back_right;
	uint8_t center;
	uint8_t subwoofer;
} Trim;

typedef struct {
	uint8_t mute:1;
	uint8_t mix_channels:1;
	uint8_t third_dimension:1;
	uint8_t tone_control:1;
	uint8_t enchance_surround:1;
	uint8_t bluetooth:1;
	uint8_t :2;
} Functions;

typedef enum __attribute__((__packed__)) {
	DIRECT = 7,
	AUX = 8,
	BLUETOOTH = 9,
	RCA1 = 10,
	RCA2 = 11,
} InputType;

typedef struct {
	uint8_t volume;
	Tones tones;
	Trim trim;
	Functions funcs;
	InputType input:8;
} Config;

typedef enum {
	FunctionMute,
	FunctionMix,
	Function3D,
	FunctionTone,
	FunctionSurround,
	FunctionBluetooth
} FunctionType;

typedef enum {
	DoSetVolume = 0x15,
	DoSetTone = 0x16,
	DoSetTrim = 0x17,
	DoSetInput = 0x18,
	DoSetFunction = 0x19,
	DoGetParams = 0x1A,
	DoGetVersion = 0x1B
} MasterCommand;

#endif//CONFIG_H
