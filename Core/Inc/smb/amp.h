/*
 * amp.h
 *
 *  Created on: Mar 5, 2022
 *      Author: symbx
 */

#ifndef INC_SMB_AMP_H_
#define INC_SMB_AMP_H_

typedef enum {
	ScreenIdle,
	ScreenGoIdle,
	ScreenLoading,
	ScreenVolume,
	ScreenTone,
	ScreenTrim,
	ScreenFunction,
	ScreenInput
} ScreenState;

typedef enum {
	ToneBass,
	ToneMiddle,
	ToneTreble
} ToneType;

typedef enum {
	SpeakerFrontLeft,
	SpeakerFrontRight,
	SpeakerBackLeft,
	SpeakerBackRight,
	SpeakerCenter,
	SpeakerSubWoofer
} SpeekerType;

void amp_load(void);
void process_input(uint8_t* buffer, uint8_t len, uint8_t origin);

#endif /* INC_SMB_AMP_H_ */
