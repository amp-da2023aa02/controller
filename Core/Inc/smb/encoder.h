/*
 * encoder.h
 *
 *  Created on: Jul 6, 2021
 *      Author: symbx
 */

#ifndef INC_SMB_ENCODER_H_
#define INC_SMB_ENCODER_H_

#include <stdint.h>


typedef void (*encoder_callback)(int8_t direction);

void encoder_init(void);
void encoder_set_callback(encoder_callback call);
void encoder_task(void);

#endif /* INC_SMB_ENCODER_H_ */
