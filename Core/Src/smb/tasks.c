#include "smb/tasks.h"

static task _tasks[TASKS_MAX];
static uint8_t _tasksTail;

void tasks_init (void) {
	_tasksTail = 0;
}

void tasks_set (func call, uint16_t delay, uint16_t interval) {
	if (!call) {
		return;
	}
	for (uint8_t i = 0; i < _tasksTail; i++) {
		if (_tasks[i].function == call) {
			_tasks[i].delay = delay;
			_tasks[i].interval = interval;
			_tasks[i].flags = (delay == 0 && interval == 0) ? FLAG_RUN : 0;
			return;
		}
	}

	if (_tasksTail < TASKS_MAX) {
		_tasks[_tasksTail].function = call;
		_tasks[_tasksTail].delay = delay;
		_tasks[_tasksTail].interval = interval;
		_tasks[_tasksTail].flags = 0;
		_tasksTail++;
	}
}

void tasks_remove (func call) {
	for (uint8_t i = 0; i < _tasksTail; i++) {
		if (_tasks[i].function == call) {
			if (_tasksTail - 1 != i) {
				_tasks[i] = _tasks[_tasksTail - 1];
			}
			_tasksTail--;
			return;
		}
	}
}

void tasks_execute (void) {
	for (uint8_t i = 0; i < _tasksTail; i++) {
		if (_tasks[i].flags == FLAG_RUN) {
			func execute = _tasks[i].function;
			if (_tasks[i].interval == 0) {
				tasks_remove(_tasks[i].function);
			} else {
				_tasks[i].flags = 0;
				if (_tasks[i].delay == 0) {
					_tasks[i].delay = _tasks[i].interval - 1;
				}
			}
			execute();
		}
	}
}

void tasks_tick (void) {
	for (uint8_t i = 0; i < _tasksTail; i++) {
		if (_tasks[i].delay == 0) {
			_tasks[i].flags = FLAG_RUN;
		} else {
			_tasks[i].delay--;
		}
	}
}
