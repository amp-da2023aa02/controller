/*
 * encoder.c
 *
 *  Created on: Mar 5, 2022
 *      Author: symbx
 */

#include "smb/encoder.h"
#include <stm32f1xx.h>
#include <stdlib.h>

uint16_t _encoder_state = 0;
encoder_callback _encoder_callback;

extern TIM_HandleTypeDef htim3;

void encoder_task(void) {
	uint16_t state = __HAL_TIM_GET_COUNTER(&htim3);
	state = 32767 - ((state - 1) & 0xFFFF) / 2;
	if(state != _encoder_state) {
		int8_t diff = _encoder_state - state;
		if (abs(diff) > 1) {
			diff /= diff;
		}
		if (_encoder_callback != 0) {
			_encoder_callback(diff);
		}
		_encoder_state = state;
	}
}

void encoder_init(void) {
	_encoder_callback = 0;
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
}

void encoder_set_callback(encoder_callback call) {
	_encoder_callback = call;
}
