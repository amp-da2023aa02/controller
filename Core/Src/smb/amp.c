/*
 * amp.c
 *
 *  Created on: Mar 5, 2022
 *      Author: symbx
 */

#include "smb/config.h"
#include "smb/tasks.h"
#include "smb/amp.h"
#include "smb/ssd1306.h"
#include "smb/encoder.h"
#include "smb/flash.h"
#include "smb/pt2322.h"
#include "smb/pt2323.h"
#include <stdlib.h>
#include <math.h>
#include "usbd_cdc_if.h"

extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;
extern UART_HandleTypeDef huart2;

ScreenState _screenState = ScreenIdle;
ToneType _tone = ToneBass;
SpeekerType _trim = SpeakerFrontLeft;
FunctionType _func = FunctionMute;
Config _cfg = {
		40, // Volume
		{0, 0, 0}, // Tones
		{0, 0, 0, 0, 0, 0}, // Trim
		{0}, // Functions
		DIRECT // end
};
uint8_t buttons = 0;
int8_t _loaderDir = 1;
uint8_t _loaderSize = 32;

static void dig2str(int8_t val, char* str) {
	uint8_t pos = 0;
	if (val < 0) {
		str[pos] = '-';
		pos++;
	}
	str[pos] = abs(val / 10) + 48;
	pos++;
	str[pos] = abs(val % 10) + 48;
}

static uint8_t tone_val(int8_t val) {
	val = val / 2;
	if (val > 0) {
		return (uint8_t) (PT2322_MAX_TONE + 1) - val;
	} else {
		return (uint8_t) (PT2322_MAX_TONE / 2) + val;
	}
}

static uint8_t trim_val(uint8_t val) {
	return PT2322_MAX_TRIM - val;
}

static void show_loader(void) {
	_loaderDir = 1;
	_loaderSize = 32;
	_screenState = ScreenLoading;
}

static uint8_t check_button(GPIO_TypeDef* gpio, uint16_t pin, uint8_t code) {
	if (HAL_GPIO_ReadPin(gpio, pin) == GPIO_PIN_RESET) {
		if ((buttons & code) != 0) {
			buttons &= ~code;
			return 1;
		}
	} else {
		buttons |= code;
	}
	return 0;
}

static inline void load_config(void) {
	read_last_data_in_flash((uint16_t*) &_cfg);
}

static void save_config(void) {
	write_to_flash((uint16_t*) &_cfg);
}

static void send_command(uint8_t addr, uint8_t cmd) {
	HAL_I2C_Master_Transmit(&hi2c1, addr,  &cmd, 1, 100);
}

static void set_input(void) {
	send_command(PT2323_ADDR, PT2323_INPUT | (_cfg.input & 0xFF));
}

static void set_volume(void) {
	uint8_t buffer[2] = {PT2322_VOL_10, PT2322_VOL_01};
	buffer[0] |= ((PT2322_MAX_VOL - _cfg.volume) / 10);
	buffer[1] |= ((PT2322_MAX_VOL - _cfg.volume) % 10);
	HAL_I2C_Master_Transmit(&hi2c1, PT2322_ADDR, (uint8_t*) buffer, 2, 100);
}

static void set_tone(void) {
	send_command(PT2322_ADDR, PT2322_TONE_BASS | tone_val(_cfg.tones.bass));
	send_command(PT2322_ADDR, PT2322_TONE_MIDDLE | tone_val(_cfg.tones.middle));
	send_command(PT2322_ADDR, PT2322_TONE_TREBLE | tone_val(_cfg.tones.treble));
}

static void set_trim(void) {
	send_command(PT2322_ADDR, PT2322_TRIM_FL| trim_val(_cfg.trim.front_left));
	send_command(PT2322_ADDR, PT2322_TRIM_FR| trim_val(_cfg.trim.front_right));
	send_command(PT2322_ADDR, PT2322_TRIM_BL| trim_val(_cfg.trim.back_left));
	send_command(PT2322_ADDR, PT2322_TRIM_BR| trim_val(_cfg.trim.back_right));
	send_command(PT2322_ADDR, PT2322_TRIM_C| trim_val(_cfg.trim.center));
	send_command(PT2322_ADDR, PT2322_TRIM_SW| trim_val(_cfg.trim.subwoofer));
}

static void set_functions(void) {
	send_command(PT2322_ADDR, PT2322_FUNCTIONS| (_cfg.funcs.mute << 3) | (_cfg.funcs.third_dimension << 2) | (_cfg.funcs.tone_control << 1));
	send_command(PT2323_ADDR, PT2323_MIX | _cfg.funcs.mix_channels);
	send_command(PT2323_ADDR, PT2323_ENCH| _cfg.funcs.enchance_surround);
}

// Render functions

static inline void render_loader(void) {
	  if (_loaderSize == 64 || _loaderSize == 0) {
		  _loaderDir *= -1;
	  }
	  _loaderSize += _loaderDir;
	ssd1306_Fill(Black);
	ssd1306_DrawHLine(64 - _loaderSize, 0, _loaderSize << 1, White);
	ssd1306_DrawHLine(_loaderSize, 63, (64 - _loaderSize) << 1, White);
	ssd1306_SetCursor(36, 14);
	ssd1306_WriteString(DEVELOPER_STR, Font_11x18, White);
	ssd1306_SetCursor(36, 32);
	ssd1306_WriteString(APPLICATION_STR, Font_11x18, White);
	ssd1306_UpdateScreen(&hi2c2);
}

static inline void render_volume(void) {
	ssd1306_Fill(Black);
	ssd1306_SetCursor(31, 1);
	ssd1306_WriteString(VOLUME_STR, Font_11x18, White);
	char line[4] = {0};
	dig2str(_cfg.volume,line);
	ssd1306_SetCursor(48, 19);
	ssd1306_WriteString(line, Font_16x26, White);
	ssd1306_UpdateScreen(&hi2c2);
}

static inline void render_tone(void) {
	ssd1306_Fill(Black);
	ssd1306_SetCursor(42, 1);
	ssd1306_WriteString(TONE_STR, Font_11x18, White);
	char line[4] = {0};
	switch (_tone) {
	case ToneBass:
		dig2str(_cfg.tones.bass, line);
		break;
	case ToneMiddle:
		dig2str(_cfg.tones.middle, line);
		break;
	case ToneTreble:
		dig2str(_cfg.tones.treble, line);
		break;
	}
	ssd1306_SetCursor(line[0] == '-' ? 40 : 48, 19);
	ssd1306_WriteString(line, Font_16x26, White);
	switch (_tone) {
	case ToneBass:
		ssd1306_SetCursor(42, 45);
		ssd1306_WriteString(BASS_STR, Font_11x18, White);
		break;
	case ToneMiddle:
		ssd1306_SetCursor(31, 45);
		ssd1306_WriteString(MIDDLE_STR, Font_11x18, White);
		break;
	case ToneTreble:
		ssd1306_SetCursor(31, 45);
		ssd1306_WriteString(TREBLE_STR, Font_11x18, White);
		break;
	}
	ssd1306_UpdateScreen(&hi2c2);
}

static inline void render_trim(void) {
	ssd1306_Fill(Black);
	ssd1306_SetCursor(42, 1);
	ssd1306_WriteString(TRIM_STR, Font_11x18, White);
	char line[4] = {0};
	switch (_trim) {
	case SpeakerFrontLeft:
		dig2str(_cfg.trim.front_left, line);
		break;
	case SpeakerFrontRight:
		dig2str(_cfg.trim.front_right, line);
		break;
	case SpeakerBackLeft:
		dig2str(_cfg.trim.back_left, line);
		break;
	case SpeakerBackRight:
		dig2str(_cfg.trim.back_right, line);
		break;
	case SpeakerCenter:
		dig2str(_cfg.trim.center, line);
		break;
	case SpeakerSubWoofer:
		dig2str(_cfg.trim.subwoofer, line);
		break;
	}
	ssd1306_SetCursor(line[0] == '-' ? 40 : 48, 19);
	ssd1306_WriteString(line, Font_16x26, White);
	if (_trim == SpeakerFrontLeft || _trim == SpeakerFrontRight ||
			_trim == SpeakerBackLeft || _trim == SpeakerBackRight) {
		ssd1306_SetCursor(3, 45);
		ssd1306_WriteString((_trim == SpeakerFrontLeft || _trim == SpeakerFrontRight) ? FRONT_STR : BACK_STR, Font_11x18, White);
		if (_trim == SpeakerFrontLeft || _trim == SpeakerBackLeft) {
			ssd1306_SetCursor(80, 45);
			ssd1306_WriteString(LEFT_STR, Font_11x18, White);
		} else {
			ssd1306_SetCursor(69, 45);
			ssd1306_WriteString(RIGHT_STR, Font_11x18, White);
		}
	} else {
		if (_trim == SpeakerCenter) {
			ssd1306_SetCursor(36, 45);
			ssd1306_WriteString(CENTER_STR, Font_11x18, White);
		} else {
			ssd1306_SetCursor(14, 45);
			ssd1306_WriteString(SUBWOOFER_STR, Font_11x18, White);
		}
	}
	ssd1306_UpdateScreen(&hi2c2);
}

static inline void render_func(void) {
	ssd1306_Fill(Black);
	uint8_t val = 0;
	switch (_func) {
	case FunctionMute:
		val = _cfg.funcs.mute;
		ssd1306_SetCursor(42, 1);
		ssd1306_WriteString(MUTE_STR, Font_11x18, White);
		break;
	case Function3D:
		val = _cfg.funcs.third_dimension;
		ssd1306_SetCursor(53, 1);
		ssd1306_WriteString(THIRD_D_STR, Font_11x18, White);
		break;
	case FunctionMix:
		val = _cfg.funcs.mix_channels;
		ssd1306_SetCursor(14, 1);
		ssd1306_WriteString(MIX_CHNL_STR, Font_11x18, White);
		break;
	case FunctionTone:
		val = _cfg.funcs.tone_control;
		ssd1306_SetCursor(14, 1);
		ssd1306_WriteString(TONE_CTRL_STR, Font_11x18, White);
		break;
	case FunctionSurround:
		val = _cfg.funcs.enchance_surround;
		ssd1306_SetCursor(25, 1);
		ssd1306_WriteString(ENCHANCE_STR, Font_11x18, White);
		break;
	case FunctionBluetooth:
		val = _cfg.funcs.bluetooth;
		ssd1306_SetCursor(14, 1);
		ssd1306_WriteString(BLUETOOTH_STR, Font_11x18, White);
		break;
	}
	if (val) {
		ssd1306_SetCursor(48, 19);
		ssd1306_WriteString(ON_STR, Font_16x26, White);
	} else {
		ssd1306_SetCursor(40, 19);
		ssd1306_WriteString(OFF_STR, Font_16x26, White);
	}
	ssd1306_UpdateScreen(&hi2c2);
}

static inline void render_input(void) {
	ssd1306_Fill(Black);
	ssd1306_SetCursor(36, 1);
	ssd1306_WriteString(INPUT_STR, Font_11x18, White);
	switch(_cfg.input) {
	case DIRECT:
		ssd1306_SetCursor(40, 19);
		ssd1306_WriteString(DIRECT_STR, Font_16x26, White);
		break;
	case BLUETOOTH:
		ssd1306_SetCursor(14, 19);
		ssd1306_WriteString(BLUETOOTH_STR, Font_11x18, White);
		break;
	case AUX:
		ssd1306_SetCursor(40, 19);
		ssd1306_WriteString(AUX_STR, Font_16x26, White);
		break;
	case RCA1:
		ssd1306_SetCursor(32, 19);
		ssd1306_WriteString(RCA1_STR, Font_16x26, White);
		break;
	case RCA2:
		ssd1306_SetCursor(32, 19);
		ssd1306_WriteString(RCA2_STR, Font_16x26, White);
		break;
	}
	ssd1306_UpdateScreen(&hi2c2);
}

static inline void render_blank(void) {
	ssd1306_Fill(Black);
	ssd1306_UpdateScreen(&hi2c2);
	_screenState = ScreenIdle;
}

static void render(void) {
	switch (_screenState) {
	case ScreenLoading:
		render_loader();
		break;
	case ScreenVolume:
		render_volume();
		break;
	case ScreenIdle:
		break;
	case ScreenTone:
		render_tone();
		break;
	case ScreenTrim:
		render_trim();
		break;
	case ScreenFunction:
		render_func();
		break;
	case ScreenGoIdle:
		render_blank();
		break;
	case ScreenInput:
		render_input();
		break;
	}
}

static void screen_clear(void) {
	_screenState = ScreenGoIdle;;
}

// UX

static void volume_rotate(int8_t direction) {
	_screenState = ScreenVolume;
	tasks_set(&screen_clear, SCREEN_CLEAR, 0);
	if (_cfg.volume > 0 && direction < 0) {
		_cfg.volume--;
	} else if (_cfg.volume < PT2322_MAX_VOL && direction > 0) {
		_cfg.volume++;
	} else {
		return;
	}
	tasks_set(&set_volume, CONFIG_APPLY, 0l);
	// TODO: Save
}

static void rotary_reset(void) {
	encoder_set_callback(&volume_rotate);
}

static void tone_rotate(int8_t direction) {
	_screenState = ScreenTone;
	tasks_set(&rotary_reset, ROTARY_RESET, 0);
	tasks_set(&screen_clear, SCREEN_CLEAR, 0);
	int8_t* val = 0;
	switch (_tone) {
	case ToneBass:
		val = &_cfg.tones.bass;
		break;
	case ToneMiddle:
		val = &_cfg.tones.middle;
		break;
	case ToneTreble:
		val = &_cfg.tones.treble;
		break;
	}
	if ((*val) % 2 == 1) {
		(*val) += ((*val) > 0) ? -1 : 1;
	}
	if ((*val) > -PT2322_MAX_TONE && direction < 0) {
		(*val) -= 2;
	} else if ((*val) < PT2322_MAX_TONE && direction > 0) {
		(*val) += 2;
	} else {
		return;
	}
	tasks_set(&set_tone, CONFIG_APPLY, 0l);
	// TODO: Save
}

static void trim_rotate(int8_t direction) {
	_screenState = ScreenTrim;
	tasks_set(&rotary_reset, ROTARY_RESET, 0);
	tasks_set(&screen_clear, SCREEN_CLEAR, 0);
	uint8_t* val = 0;
	switch (_trim) {
	case SpeakerFrontLeft:
		val = &_cfg.trim.front_left;
		break;
	case SpeakerFrontRight:
		val = &_cfg.trim.front_right;
		break;
	case SpeakerBackLeft:
		val = &_cfg.trim.back_left;
		break;
	case SpeakerBackRight:
		val = &_cfg.trim.back_right;
		break;
	case SpeakerCenter:
		val = &_cfg.trim.center;
		break;
	case SpeakerSubWoofer:
		val = &_cfg.trim.subwoofer;
		break;
	}
	if ((*val) > 0 && direction < 0) {
		(*val)--;
	} else if ((*val) < 15 && direction > 0) {
		(*val)++;
	} else {
		return;
	}
	tasks_set(&set_trim, CONFIG_APPLY, 0l);
	// TODO: Save
}

static void buttons_process(void) {
	if (check_button(GPIOB, GPIO_PIN_13, BUTTON_0) != 0) {
		tasks_set(&rotary_reset, ROTARY_RESET, 0);
		tasks_set(&screen_clear, SCREEN_CLEAR, 0);
		if (_screenState == ScreenTone) {
			switch (_tone) {
			case ToneBass:
				_tone = ToneMiddle;
				break;
			case ToneMiddle:
				_tone = ToneTreble;
				break;
			case ToneTreble:
				_tone = ToneBass;
				break;
			}
		} else {
			_tone = ToneBass;
			_screenState = ScreenTone;
		}
		encoder_set_callback(&tone_rotate);
	} else if (check_button(GPIOB, GPIO_PIN_14, BUTTON_1) != 0) {
		tasks_set(&rotary_reset, ROTARY_RESET, 0);
		tasks_set(&screen_clear, SCREEN_CLEAR, 0);
		if (_screenState == ScreenTrim) {
			switch (_trim) {
			case SpeakerFrontLeft:
				_trim = SpeakerFrontRight;
				break;
			case SpeakerFrontRight:
				_trim = SpeakerBackLeft;
				break;
			case SpeakerBackLeft:
				_trim = SpeakerBackRight;
				break;
			case SpeakerBackRight:
				_trim = SpeakerCenter;
				break;
			case SpeakerCenter:
				_trim = SpeakerSubWoofer;
				break;
			case SpeakerSubWoofer:
				_trim = SpeakerFrontLeft;
				break;
			}
		} else {
			_trim = SpeakerFrontLeft;
			_screenState = ScreenTrim;
		}
		encoder_set_callback(&trim_rotate);
	} else if (check_button(GPIOB, GPIO_PIN_15, BUTTON_2) != 0) {
		tasks_set(&screen_clear, SCREEN_CLEAR, 0);
		_screenState = ScreenFunction;
		switch (_func) {
		case FunctionMute:
			_func = Function3D;
			break;
		case Function3D:
			_func = FunctionMix;
			break;
		case FunctionMix:
			_func = FunctionTone;
			break;
		case FunctionTone:
			_func = FunctionSurround;
			break;
		case FunctionSurround:
			_func = FunctionBluetooth;
			break;
		case FunctionBluetooth:
			_func = FunctionMute;
			break;
		}
	} else if (check_button(GPIOA, GPIO_PIN_8, BUTTON_3) != 0) {
		tasks_set(&screen_clear, SCREEN_CLEAR, 0);
		if (_screenState == ScreenFunction) {
			switch (_func) {
			case FunctionMute:
				_cfg.funcs.mute = _cfg.funcs.mute ? 0 : 1;
				break;
			case Function3D:
				_cfg.funcs.third_dimension = _cfg.funcs.third_dimension ? 0 : 1;
				break;
			case FunctionMix:
				_cfg.funcs.mix_channels = _cfg.funcs.mix_channels ? 0 : 1;
				break;
			case FunctionTone:
				_cfg.funcs.tone_control = _cfg.funcs.tone_control ? 0 : 1;
				break;
			case FunctionSurround:
				_cfg.funcs.enchance_surround = _cfg.funcs.enchance_surround ? 0 : 1;
				break;
			case FunctionBluetooth:
				_cfg.funcs.bluetooth = _cfg.funcs.bluetooth ? 0 : 1;
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, _cfg.funcs.bluetooth ? GPIO_PIN_SET : GPIO_PIN_RESET);
				// TODO: Save
				return;
			default:
				return;
			}
			tasks_set(&set_functions, CONFIG_APPLY, 0);
			// TODO: Save
		}
	} else if (check_button(GPIOA, GPIO_PIN_9, BUTTON_4) != 0) {
		tasks_set(&screen_clear, SCREEN_CLEAR, 0);
		if (_screenState == ScreenInput) {
			switch (_cfg.input) {
			case DIRECT:
				_cfg.input = BLUETOOTH;
				break;
			case BLUETOOTH:
				_cfg.input = AUX;
				break;
			case AUX:
				_cfg.input = RCA1;
				break;
			case RCA1:
				_cfg.input = RCA2;
				break;
			//case RCA2:
			default:
				_cfg.input = DIRECT;
				break;
			}
			tasks_set(&set_input, CONFIG_APPLY, 0);
			// TODO: Save
		} else {
			_screenState = ScreenInput;
		}
	}
}

void process_input(uint8_t* buffer, uint8_t len, uint8_t origin) {
	if (buffer[0] != 0xAA) { // Header
		return;
	}
	MasterCommand cmd = buffer[1];
	switch (cmd) {
	case DoSetVolume:
		if (len != 3) {
			return;
		}
		_cfg.volume = MIN(PT2322_MAX_VOL, buffer[2]);
		tasks_set(&set_volume, CONFIG_APPLY, 0);
		break;
	case DoSetTone:
		if (len != 5) {
			return;
		}
		_cfg.tones.bass = (int8_t) (MIN(28, buffer[2]) - 14);
		_cfg.tones.middle = (int8_t) (MIN(28, buffer[3]) - 14);
		_cfg.tones.treble = (int8_t) (MIN(28, buffer[4]) - 14);
		tasks_set(&set_tone, CONFIG_APPLY, 0);
		break;
	case DoSetTrim:
		if (len != 8) {
			return;
		}
		_cfg.trim.front_left = MIN(15, buffer[2]);
		_cfg.trim.front_right = MIN(15, buffer[3]);
		_cfg.trim.back_left = MIN(15, buffer[4]);
		_cfg.trim.back_right = MIN(15, buffer[5]);
		_cfg.trim.center = MIN(15, buffer[6]);
		_cfg.trim.subwoofer = MIN(15, buffer[7]);
		tasks_set(&set_trim, CONFIG_APPLY, 0);
		break;
	case DoSetInput:
		if (len != 3) {
			return;
		}
		_cfg.input = MIN(11, MAX(7, buffer[2]));
		tasks_set(&set_input, CONFIG_APPLY, 0);
		break;
	case DoSetFunction:
		if (len != 3) {
			return;
		}
		_cfg.funcs.mute = buffer[2] & 0x01;
		_cfg.funcs.mix_channels = (buffer[2] >> 1) & 0x01;
		_cfg.funcs.third_dimension = (buffer[2] >> 2) & 0x01;
		_cfg.funcs.tone_control = (buffer[2] >> 3) & 0x01;
		_cfg.funcs.enchance_surround = (buffer[2] >> 4) & 0x01;
		_cfg.funcs.bluetooth = (buffer[2] >> 5) & 0x01;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, _cfg.funcs.bluetooth ? GPIO_PIN_SET : GPIO_PIN_RESET);
		tasks_set(&set_functions, CONFIG_APPLY, 0);
		break;
	case DoGetParams:
		if (len != 2) {
			return;
		}
		{
			uint8_t buffer[14];
			buffer[0] = 0xAA;
			buffer[1] = 0x85;
			memcpy(buffer + 2, &_cfg, 12);
			if (origin == 0) {
				CDC_Transmit_FS(buffer, 14);
			} else {
				HAL_UART_Transmit(&huart2, buffer, 14, 100);
			}
		}
		return;
	case DoGetVersion:
		if (len != 2) {
			return;
		}
		{
			uint8_t buffer[5] = {0xAA, 0x86, 0, 0, 1};
			if (origin == 0) {
				CDC_Transmit_FS(buffer, 5);
			} else {
				HAL_UART_Transmit(&huart2, buffer, 5, 100);
			}
		}
		return;
	default:
		return;
	}
	// TODO: Save
}

static void wait_ready(void) {
	if (HAL_I2C_IsDeviceReady(&hi2c1, PT2322_ADDR, 1, 100) != HAL_OK) {
		tasks_set(&wait_ready, 1000, 0);
		return;
	}
	uint8_t buffer[2];
	buffer[0] = PT2322_SYS_RESET;
	HAL_I2C_Master_Transmit(&hi2c1, PT2322_ADDR, buffer, 1, 100);
	buffer[0] = PT2322_ACTIVATE;
	HAL_I2C_Master_Transmit(&hi2c1, PT2322_ADDR, buffer, 1, 100);
	set_tone();
	set_trim();
	set_functions();
	set_input();
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET); // Run
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
	tasks_set(&screen_clear, SCREEN_CLEAR, 0);
}

uint8_t _bt_char;
uint8_t _bt_buffer[14];
uint8_t _bt_pos = 0;
static void bt_end_read(void) {
	if (_bt_pos > 0) {
		process_input(_bt_buffer, _bt_pos, 1);
		memset(_bt_buffer, 0, 14);
		_bt_pos = 0;
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if (_bt_pos == 0 && _bt_char != 0xAA) {
		HAL_UART_Receive_IT(&huart2, &_bt_char, 1);
		return;
	}
	tasks_set(&bt_end_read, 50, 0);
	_bt_buffer[_bt_pos] = _bt_char;
	_bt_pos++;
	if (_bt_pos == 14) {
		memset(_bt_buffer, 0, 14);
		_bt_pos = 0;
	}
	HAL_UART_Receive_IT(&huart2, &_bt_char, 1);
}

void amp_load(void) {
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET); // StandBy
	ssd1306_Init(&hi2c2);
	ssd1306_Fill(Black);
	ssd1306_UpdateScreen(&hi2c2);
	tasks_init();
	show_loader();
	encoder_init();
	encoder_set_callback(&volume_rotate);
	HAL_UART_Receive_IT(&huart2, &_bt_char, 1);

	// TODO: Load config

	if (_cfg.input < 7 || _cfg.input > 11) {
		_cfg.input = DIRECT;
	}
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, _cfg.funcs.bluetooth ? GPIO_PIN_SET : GPIO_PIN_RESET);
	tasks_set(&encoder_task, 20, 20);
	tasks_set(&buttons_process, 3, 3);
	tasks_set(&render, 16, 16);
	// tasks_set(&screen_clear, SCREEN_CLEAR, 0);

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
	tasks_set(&wait_ready, 1000, 0);
}
